api = 2
core = 7.x

; Pull the latest Drupal core
projects[drupal][type] = core
projects[drupal][version] = 7.38
projects[drupal][download][type] = get
projects[drupal][download][url] = http://ftp.drupal.org/files/projects/drupal-7.38.tar.gz

; Uportal Profile
projects[uportal][type] = profile
projects[uportal][download][type] = "git"
projects[uportal][download][url] = "http://git.drupal.org/sandbox/benjaminug/2535410.git"
projects[uportal][download][branch] = "7.x-1.x"
